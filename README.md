# mdm

## Project setup

Install all the dependencies and node modules using the below npm command before setting a development/production environment.
```
npm install
```

To Compile and hot-reload for development. 
```
npm run serve 
```

 To Compile and minifies for production mode
```
npm run build
```

### Test System

To perform all the end to end tests
```
npm run test:e2e
```

To run the unit tests
```
npm run test:unit
```

### Setup on staging-mum machine

Create a build of using ``` npm run build ``` which results in a dist folder with all the production/minified build code. Copy the dist folder to ** /var/www/html/mdm_2.0 ** (remove all the files under mdm_2.0 before copying). Restart the nginx using ``` sudo /etc/init.d/nginx restart ``` 

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
