import colors from 'vuetify/lib/util/colors'

export default {
  primary: {
    base: colors.blue.base,
    darken1: colors.purple.darken2,
  },
  secondary: colors.blue.lighten5,
  // All keys will generate theme styles,
  // Here we add a custom `tertiary` color
  tertiary: colors.pink.base,
}