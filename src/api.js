import axios from 'axios';

import axiosHelper from './apiHelper'

const API = axios.create({
  baseURL: 'https://mdm.tekpea.com',
  // baseURL: 'http://localhost:3000',
  withCredentials: true,
  headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
  }
});
API.interceptors.request.use(
  request => {
    /** TODO: Add any request interceptors */
    return request;
  },
  error => {
    /** TODO: Do something with response error */
    return Promise.reject(error);
  }
);

API.interceptors.response.use(
  response => {
    /** TODO: Add any response interceptors */
    return response;
  },
  error => {
    /** TODO: Do something with response error */
    return Promise.reject(error);
  }
);

export default API;