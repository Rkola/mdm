import { Pie, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
  extends: Pie,
  mixins: [reactiveProp],
  props: ['options'],
  mounted () {
    this.renderChart(this.chartData, this.options)
  }
}
// import { Bar } from 'vue-chartjs'
 
// export default {
//   extends: Bar,
//   mounted () {
//     // Overwriting base render method with actual data.
//     this.renderChart({
//       labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
//       datasets: [
//         {
//           label: '$',
//           backgroundColor: '#05CBE1',
//           data: [40, 20, 12, 39, 10, 40, 39, 50, 40, 20, 12, 21]
//         }
//       ]
//     })
//   }
// }