import Vue from 'vue'
// import './plugins/vuetify'
import vuetify from './plugins/vuetify'
import "./stylus/main.styl"
import App from './App.vue'
import router from './router'
import store from './store/store'
import i18n from './plugins/i18'


Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: h => h(App)
}).$mount('#app')
